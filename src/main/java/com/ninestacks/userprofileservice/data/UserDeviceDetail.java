package com.ninestacks.userprofileservice.data;

import java.io.Serializable;

public class UserDeviceDetail implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 236692178800579351L;
	
	private String deviceToken;

	private String os;
	
	private String networkStatus;
	
	private String appVersion;
	
	private String deviceDensity;
	
	private String deviceHeight;
	
	private String deviceWidth;
	
    private String advId;
    
    private String osId;

	public String getDeviceToken() {
		return deviceToken;
	}

	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}
	
	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public String getNetworkStatus() {
		return networkStatus;
	}

	public void setNetworkStatus(String networkStatus) {
		this.networkStatus = networkStatus;
	}

	public String getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

	public String getDeviceDensity() {
		return deviceDensity;
	}

	public void setDeviceDensity(String deviceDensity) {
		this.deviceDensity = deviceDensity;
	}

	public String getDeviceHeight() {
		return deviceHeight;
	}

	public void setDeviceHeight(String deviceHeight) {
		this.deviceHeight = deviceHeight;
	}

	public String getDeviceWidth() {
		return deviceWidth;
	}

	public void setDeviceWidth(String deviceWidth) {
		this.deviceWidth = deviceWidth;
	}

	public String getAdvId() {
		return advId;
	}

	public void setAdvId(String advId) {
		this.advId = advId;
	}

	public String getOsId() {
		return osId;
	}

	public void setOsId(String osId) {
		this.osId = osId;
	}

	
}
