package com.ninestacks.userprofileservice.data;

public class UserProfileRequest extends AbstractRequest {
	 private static final long serialVersionUID = 1L;
	    private String userId;
	    private String name;
	    private String password;
	    private String emailAddress;
	    private String phoneNumber;
	    private String gender;
	    private String dateOfBirth;  // in DD-MM-YYYY format
	    private Boolean isOtpVerified;
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}
		public String getEmailAddress() {
			return emailAddress;
		}
		public void setEmailAddress(String emailAddress) {
			this.emailAddress = emailAddress;
		}
		public String getPhoneNumber() {
			return phoneNumber;
		}
		public void setPhoneNumber(String phoneNumner) {
			this.phoneNumber = phoneNumner;
		}
		
		public String getGender() {
			return gender;
		}
		public void setGender(String gender) {
			this.gender = gender;
		}
		public String getDateOfBirth() {
			return dateOfBirth;
		}
		public void setDateOfBirth(String dateOfBirth) {
			this.dateOfBirth = dateOfBirth;
		}
		public String getUserId() {
			return userId;
		}
		public void setUserId(String userId) {
			this.userId = userId;
		}
		public Boolean getIsOtpVerified() {
			return isOtpVerified;
		}
		public void setIsOtpVerified(Boolean isOtpVerified) {
			this.isOtpVerified = isOtpVerified;
		}
	    
	    

}
