package com.ninestacks.userprofileservice.data;

public class LoginRequest extends AbstractRequest  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5305988575789669403L;

	private String userId;
	private String emailAddress;
	private String password;
    private String phoneNumber;
    private Long expiryInMinutes;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public Long getExpiryInMinutes() {
		return expiryInMinutes;
	}
	public void setExpiryInMinutes(Long expiryInMinutes) {
		this.expiryInMinutes = expiryInMinutes;
	}
    
    
}
