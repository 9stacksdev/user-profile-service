package com.ninestacks.userprofileservice.data;

import java.io.Serializable;


public class SocialNetworkDetail implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private String id;
    
    private String source;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
    
}
