package com.ninestacks.userprofileservice.validator;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.ninestacks.userprofileservice.constants.UserProfileServiceConstants;
import com.ninestacks.userprofileservice.data.UserProfileRequest;
import com.ninestacks.userprofileservice.validator.enums.CommonValidationUtil;
import com.ninestacks.userprofileservice.validator.enums.DateUtil;
import com.ninestacks.userprofileservice.validator.enums.Gender;


@Component
public class UserProfileRequestValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        if (clazz.equals(UserProfileRequest.class)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void validate(Object target, Errors errors) {
    	UserProfileRequest userProfileRequest = (UserProfileRequest) target;

        ValidationUtils.rejectIfEmpty(errors, "createdBy", "value.required", "createdBy cannot be blank");

        if (userProfileRequest.getPassword() != null && !CommonValidationUtil.isValidPassword(userProfileRequest.getPassword())) {
            errors.rejectValue("password", "value.required", "Invalid password");
        }
        
        if (StringUtils.isBlank(userProfileRequest.getEmailAddress()) && StringUtils.isBlank(userProfileRequest.getPhoneNumber())) {
            errors.rejectValue("primaryEmailAddress", "value.required", "Both EmailAddress & PhoneNumber cannot be blank");
        }
        
        if (StringUtils.isNotBlank(userProfileRequest.getEmailAddress())
                && !CommonValidationUtil.isValidEmailAddress(userProfileRequest.getEmailAddress())) {
            errors.rejectValue("primaryEmailAddress", "value.required", "Invalid primaryEmailAddress");
        }
        
        if (StringUtils.isNotBlank(userProfileRequest.getPhoneNumber())
                && !CommonValidationUtil.isValidPhoneNumber(userProfileRequest.getPhoneNumber())) {
            errors.rejectValue("PhoneNumber", "value.required", "Invalid PhoneNumber");
        }
        
        if (StringUtils.isNotBlank(userProfileRequest.getDateOfBirth())
                && !DateUtil.isDateValidByFormat(userProfileRequest.getDateOfBirth(), UserProfileServiceConstants.DOB_FORMAT)) {
            errors.rejectValue("dateOfBirth", "value.required", "Invalid dateOfBirth");
        }
        
        if (StringUtils.isNotBlank(userProfileRequest.getGender())
                && !Gender.getAllValues().contains(userProfileRequest.getGender())) {
            errors.rejectValue("gender", "value.required", "Invalid gender");
        }
        
      
    }
}
