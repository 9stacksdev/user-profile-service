package com.ninestacks.userprofileservice.validator.enums;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public enum MaritalStatus {
	
	MARRIED("married"),
	
	UNMARRIED("unmarried"),
	
	WIDOWED("widowed"),
	
	DIVORCED("divorced");
	
	private String value;
    
    private static final Map<String, MaritalStatus> lookup = new HashMap<>();
        
    private MaritalStatus(String value) {
        this.value = value;
    }
    
    public String getValue() {
        return value;
    }
    
    public static MaritalStatus get(String value) {
        return lookup.get(value);
    }
    
    public static List<String> getAllValues() {
        return new ArrayList<>(lookup.keySet());
    }
    
    static {
        for (final MaritalStatus t : EnumSet.allOf(MaritalStatus.class)) {
            lookup.put(t.getValue(), t);
        }
    }

}
