package com.ninestacks.userprofileservice.validator.enums;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public enum RequestSource {
	
	WEB("web"),
	
	MOBILE("mobile"),
	
	CS("cs"),
	
	CREDIT_PLATFORM("creditplatform"),
	
	OMS("oms"),
	
	PARTNER_PLATFORM("partnerplatform") ,

	ANDROID("android"),
	
	IOS("ios"),
	
	DELIVERY_MANAGER("deliveryManager");
	
	
	
	
	private String value;
    
    private static final Map<String, RequestSource> lookup = new HashMap<>();
        
    private RequestSource(String value) {
        this.value = value;
    }
    
    public String getValue() {
        return value;
    }
    
    public static RequestSource get(String value) {
        return lookup.get(value);
    }
    
    public static List<String> getAllValues() {
        return new ArrayList<>(lookup.keySet());
    }
    
    static {
        for (final RequestSource t : EnumSet.allOf(RequestSource.class)) {
            lookup.put(t.getValue(), t);
        }
    }

}
