package com.ninestacks.userprofileservice.validator.enums;


import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public enum Gender {
	
	MALE("male"),
	
	FEMALE("female"),
	
	NEUTRAL("neutral");
	
	private String value;
    
    private static final Map<String, Gender> lookup = new HashMap<>();
        
    private Gender(String value) {
        this.value = value;
    }
    
    public String getValue() {
        return value;
    }
    
    public static Gender get(String value) {
        return lookup.get(value);
    }
    
    public static List<String> getAllValues() {
        return new ArrayList<>(lookup.keySet());
    }
    
    static {
        for (final Gender t : EnumSet.allOf(Gender.class)) {
            lookup.put(t.getValue(), t);
        }
    }

}
