package com.ninestacks.userprofileservice.validator.enums;

import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import com.ninestacks.userprofileservice.constants.UserProfileServiceConstants;
import com.ninestacks.userprofileservice.exceptions.InvalidArgumentException;


public class CommonValidationUtil {
    
    public static boolean isValidEmailAddresses(Set<String> emailAddresses) {
        if (emailAddresses == null) {
            return false;
        }
        Boolean isValid = true;
        for (String email : emailAddresses) {
            if(!isValidEmailAddress(email)) {
                isValid = false;
                break;
            }
        }
        return isValid;
    }
    
    public static boolean isValidPhoneNumbers(Set<String> phoneNumbers) {
        if (phoneNumbers == null) {
            return false;
        }
        Boolean isValid = true;
        for (String phoneNumber : phoneNumbers) {
            if(!isValidPhoneNumber(phoneNumber)) {
                isValid = false;
                break;
            }
        }
        return isValid;
    }

    public static boolean isValidEmailAddress(String email) {
        if (StringUtils.isBlank(email)) {
            return false;
        }
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }
    
    public static boolean isValidPhoneNumber(String phoneNumber) {
        if (StringUtils.isBlank(phoneNumber)) {
            return false;
        }
        String ePattern = "\\d{10}";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(phoneNumber);
        return m.matches();
    }
    
    public static void validatePassword(String password) throws Exception {
        if (StringUtils.isBlank(password) || password.contains("^") || password.length() <  UserProfileServiceConstants.MIN_PASSWORD_LENGTH || !password.matches(".*[a-zA-Z0-9]+.*")) {
            throw new InvalidArgumentException("Invalid Password. Minimum length[" + UserProfileServiceConstants.MIN_PASSWORD_LENGTH + "] required.");
        }
    }
    
    public static boolean isValidPassword(String password) {
        if (StringUtils.isBlank(password) || password.contains("^") || password.length() <  UserProfileServiceConstants.MIN_PASSWORD_LENGTH || !password.matches(".*[a-zA-Z0-9]+.*")) {
            return false;
        }
        return true;
    }
    
    
}
