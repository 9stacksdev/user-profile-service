package com.ninestacks.userprofileservice.validator.enums;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public enum Profession {
	
	STUDENT("student"),
	
	PROFESSIONAL("professional"),
	
	BUSINESS_OWNER("businessowner"),
	
	OTHER("other");
	
	private String value;
    
    private static final Map<String, Profession> lookup = new HashMap<>();
        
    private Profession(String value) {
        this.value = value;
    }
    
    public String getValue() {
        return value;
    }
    
    public static Profession get(String value) {
        return lookup.get(value);
    }
    
    public static List<String> getAllValues() {
        return new ArrayList<>(lookup.keySet());
    }
    
    static {
        for (final Profession t : EnumSet.allOf(Profession.class)) {
            lookup.put(t.getValue(), t);
        }
    }

}
