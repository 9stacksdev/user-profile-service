package com.ninestacks.userprofileservice.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.BindException;
import org.springframework.validation.ObjectError;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.ninestacks.userprofileservice.data.AbstractRequest;
import com.ninestacks.userprofileservice.exceptions.InvalidArgumentException;

@Component
public class GenericValidator {

    public void validateRequest(final Validator validator, final AbstractRequest request) throws InvalidArgumentException {
        final BindException errors = new BindException(request, request.getClass().getName());
        boolean isRequestContainsError = false;
        ValidationUtils.invokeValidator(validator, request, errors);
        StringBuilder errorMessages = new StringBuilder(); 
        if (errors.hasErrors()) {
            isRequestContainsError = true;
            int errorListSize = errors.getAllErrors().size();
            for (ObjectError error : errors.getAllErrors()) {
                 errorMessages.append(error.getDefaultMessage());
                 if (errorListSize > 1) {
                     errorMessages.append(";");
                 }
                 errorListSize--;
             }
        }
        if(isRequestContainsError){
            throw new InvalidArgumentException(errorMessages.toString());
        }
    }
}
