package com.ninestacks.userprofileservice.transform;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ninestacks.userprofileservice.data.UserProfileRequest;
import com.ninestacks.userprofileservice.entity.UserProfile;
import com.ninestacks.userprofileservice.utils.DateUtils;
import com.ninestacks.userprofileservice.utils.EncryptionDecryptionUtil;


@Component
public class UserTransformer {
	
	public UserProfile transformUserProfileRequestToUserProfileEntity(UserProfileRequest userProfileRequest) throws Exception {
	UserProfile userProfile = new UserProfile();
		
		if (StringUtils.isNotBlank(userProfileRequest.getName())) {
			userProfile.setName(StringUtils.trim(userProfileRequest.getName()));
		}
		if(StringUtils.isNotBlank(userProfileRequest.getUserId())){
			userProfile.setUserId(StringUtils.trim(userProfileRequest.getUserId()));
		}

		if (userProfileRequest.getPassword() != null) {
			userProfile.setPassword(EncryptionDecryptionUtil.encode(userProfileRequest.getPassword()));
		} 
		userProfile.setEmailAddress(StringUtils.lowerCase(StringUtils.trim(userProfileRequest.getEmailAddress())));
		userProfile.setPhoneNumber(StringUtils.trim(userProfileRequest.getPhoneNumber()));

		userProfile.setGender(StringUtils.trim(userProfileRequest.getGender()));


		userProfile.setDateOfBirth(DateUtils.getUnixTimestamp(userProfileRequest.getDateOfBirth(), "dd-MM-yyyy"));

		
		userProfile.setIsValidated(userProfileRequest.getIsOtpVerified());
		
	
	return userProfile;
}
    
}
