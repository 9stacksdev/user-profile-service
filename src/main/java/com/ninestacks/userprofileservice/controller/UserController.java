package com.ninestacks.userprofileservice.controller;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ninestacks.userprofileservice.controller.response.ErrorResponse;
import com.ninestacks.userprofileservice.controller.response.LoginResponse;
import com.ninestacks.userprofileservice.controller.response.UserIdResponse;
import com.ninestacks.userprofileservice.data.LoginRequest;
import com.ninestacks.userprofileservice.data.UserProfileRequest;
import com.ninestacks.userprofileservice.entity.UserProfile;
import com.ninestacks.userprofileservice.exceptions.EntityAlreadyExistsException;
import com.ninestacks.userprofileservice.exceptions.InvalidArgumentException;
import com.ninestacks.userprofileservice.exceptions.InvalidPasswordException;
import com.ninestacks.userprofileservice.service.IUserProfileService;
import com.ninestacks.userprofileservice.utils.ResponseUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(value = "user", description = "User Profile API")
@RestController
@RequestMapping(value = "/v1/api/user")
public class UserController {

	private Logger logger = LoggerFactory.getLogger(UserController.class);

	@Autowired
	private IUserProfileService userProfileService;

	@ApiOperation(value = "Add a new user into the system")
	@RequestMapping(method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public HttpEntity<Object> addUser(@RequestBody(required = true) UserProfileRequest userProfileRequest,
			HttpServletResponse response) {
		logger.info("Entered in addUser with userProfileRequest" + userProfileRequest.toString());
		HttpHeaders headers = new HttpHeaders();
		HttpStatus status = HttpStatus.OK;
		headers.setContentType(MediaType.APPLICATION_JSON);
		try {
			String customerId = userProfileService.addUser(userProfileRequest);
			if (StringUtils.isBlank(customerId)) {
				logger.error("Error occured while adding user into the system");
				throw new Exception();
			}
			return ResponseUtils.sendResponse(new UserIdResponse(customerId), headers, status);
		} catch (InvalidArgumentException | EntityAlreadyExistsException e) {
			logger.error("InvalidArgumentException in addUser with userProfileRequest" + userProfileRequest.toString(),
					e.getMessage());
			status = HttpStatus.BAD_REQUEST;
			return ResponseUtils.sendResponse(new ErrorResponse(e.getMessage()), headers, status);
		} catch (Exception e) {
			logger.error("Error in addUser with userProfileRequest" + userProfileRequest.toString(), e);
			status = HttpStatus.INTERNAL_SERVER_ERROR;
			return ResponseUtils.sendResponse(new ErrorResponse(e.getMessage()), headers, status);
		}
	}

	@ApiOperation(value = "User Login into the system")
	@RequestMapping(path = "/login", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public HttpEntity<Object> loginUser(@RequestBody(required = true) LoginRequest loginRequest,
			HttpServletResponse response) {
		logger.info("Entered in login with loginRequest" + loginRequest.getUserId() + loginRequest.getEmailAddress()
				+ loginRequest.getPhoneNumber());
		HttpHeaders headers = new HttpHeaders();
		HttpStatus status = HttpStatus.OK;
		headers.setContentType(MediaType.APPLICATION_JSON);
		try {
			String accessToken = userProfileService.loginUser(loginRequest);
			if (StringUtils.isBlank(accessToken)) {
				logger.error("Error occured while login user into the system");
				throw new Exception();
			}
			return ResponseUtils.sendResponse(new LoginResponse(accessToken), headers, status);
		} catch (InvalidPasswordException e) {
			logger.error("InvalidPasswordException in login  with loginRequest" + loginRequest.toString(),
					e.getMessage());
			status = HttpStatus.FORBIDDEN;
			return ResponseUtils.sendResponse(new ErrorResponse(e.getMessage()), headers, status);
		} catch (InvalidArgumentException e) {
			logger.error("InvalidArgumentException in login  with loginRequest" + loginRequest.toString(),
					e.getMessage());
			status = HttpStatus.BAD_REQUEST;
			return ResponseUtils.sendResponse(new ErrorResponse(e.getMessage()), headers, status);
		} catch (Exception e) {
			logger.error("Error in login with loginRequest" + loginRequest.toString(), e);
			status = HttpStatus.INTERNAL_SERVER_ERROR;
			return ResponseUtils.sendResponse(new ErrorResponse(e.getMessage()), headers, status);
		}
	}

	@ApiOperation(value = "Validate an access token. Gives error if acessToken has been expired.")
	@RequestMapping(value = "/validate", method = RequestMethod.GET, produces = "application/json")
	public HttpEntity<Object> validateAccessToken(
			final @ApiParam(name = "userId", value = "User Id(Mandatory)") @RequestParam(value = "userId", required = true) String userId,
			final @ApiParam(name = "accessToken", value = "accessToken(Mandatory)") @RequestParam(value = "accessToken", required = true) String accessToken,
			HttpServletResponse response) {
		logger.info("Entered in validateAccessToken with userId:" + userId + ", source:" + "app" + "accessToken:"
				+ accessToken);
		HttpHeaders headers = new HttpHeaders();
		HttpStatus status = HttpStatus.OK;
		headers.setContentType(MediaType.APPLICATION_JSON);
		try {
			boolean isValid = userProfileService.validateAccessToken(userId, "app", accessToken);
			logger.info("Exited from validateAccessToken with userId:" + userId + ", source:" + "app" + "accessToken:"
					+ accessToken + ", isValid:" + isValid);
			return ResponseUtils.sendResponse(Boolean.TRUE, headers, status);
		} catch (InvalidArgumentException e) {
			logger.error("InvalidArgumentException in validateAccessToken for userId:" + userId + ", source:" + "app",
					e);
			status = HttpStatus.BAD_REQUEST;
			return ResponseUtils.sendResponse(new ErrorResponse(e.getMessage()), headers, status);
		} catch (Exception e) {
			logger.error("Error in validateAccessToken for userId:" + userId + ", source:" + "app", e);
			status = HttpStatus.INTERNAL_SERVER_ERROR;
			return ResponseUtils.sendResponse(new ErrorResponse(e.getMessage()), headers, status);
		}
	}

	@RequestMapping(value = "/{userprofileId}", method = RequestMethod.GET, produces = "application/json")
	public HttpEntity<Object> getUserById(
			final @ApiParam(name = "userId", value = "USer Id") @PathVariable("userprofileId") String userprofileId,
			HttpServletResponse response) {
		logger.info("Entered in getUserById with userprofileId[" + userprofileId + "]");
		HttpHeaders headers = new HttpHeaders();
		HttpStatus status = HttpStatus.OK;
		headers.setContentType(MediaType.APPLICATION_JSON);
		try {
			UserProfile userProfile = userProfileService.getUserById(userprofileId);
			if (userProfile == null) {
				status = HttpStatus.NOT_FOUND;
				return ResponseUtils.sendResponse(new ErrorResponse("User doesnt exist"), headers, status);
			} else {
				return ResponseUtils.sendResponse(userProfile, headers, status);
			}
		} catch (Exception e) {
			logger.error("Error in getUserById with userprofileId[" + userprofileId + "]", e);
			status = HttpStatus.INTERNAL_SERVER_ERROR;
			return ResponseUtils.sendResponse(new ErrorResponse(e.getMessage()), headers, status);
		}

	}
}