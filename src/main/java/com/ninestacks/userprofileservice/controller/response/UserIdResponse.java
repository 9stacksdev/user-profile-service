package com.ninestacks.userprofileservice.controller.response;

public class UserIdResponse {
	
	
 public UserIdResponse() {
		super();
	}

public UserIdResponse(String userId) {
		super();
		this.userId = userId;
	}

private String userId;
 public void setUserId(String userId) {
	this.userId = userId;
}
 
public String getUserId() {
	return userId;
}
}
 
