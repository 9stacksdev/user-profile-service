package com.ninestacks.userprofileservice.controller.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true)
public class ErrorResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    private String errorMsg;
    
    private String errorCode;

    public ErrorResponse() {
        super();
    }

    public ErrorResponse(String errorMsg) {
        super();
        this.errorMsg = errorMsg;
    }
    
    public ErrorResponse(String errorMsg, String errorCode) {
        super();
        this.errorMsg = errorMsg;
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    @Override
    public String toString() {
        return "ErrorResponseVO [errorMsg=" + errorMsg + "]";
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

}
