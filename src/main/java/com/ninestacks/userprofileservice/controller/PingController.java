package com.ninestacks.userprofileservice.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ninestacks.userprofileservice.repository.PingRepository;


@RestController
public class PingController {
    
	final Logger logger = LoggerFactory.getLogger(PingController.class);
    
	   @Autowired
	    private PingRepository pingRepository;

	@RequestMapping(value = "/ping", method = RequestMethod.GET)
	public String ping() {
		logger.info("Ping Controller");
	    return "Count of records in ping :"+pingRepository.count();
	}

}
