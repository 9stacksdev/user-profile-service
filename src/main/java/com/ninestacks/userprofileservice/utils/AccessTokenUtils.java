package com.ninestacks.userprofileservice.utils;

import java.util.Date;
import java.util.UUID;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;

public class AccessTokenUtils {

	public static String createAccessTokenId(String userId,String source) {
		StringBuilder accessTokenId = new StringBuilder();
		return accessTokenId.append(source).append(":").append(userId).toString();
	}

	public static String getUserId(String accessTokenId) {
		return accessTokenId.split(":")[1];
	}

	public static String createAccessTokenValue() {
		return UUID.randomUUID().toString();
	}

	public static Long getCurrentUnixTimeStamp() {
		return (new Date()).getTime() / 1000;
	}

	public static Long addSecondsToUnixTimestamp(Long currentUnixTimestamp, long seconds) {
		return ((currentUnixTimestamp * 1000) + (seconds * 1000)) / 1000;
	}

	public static String formName(String fname, String lname) {
		StringBuilder fullName = new StringBuilder(fname);
		if (StringUtils.isNotBlank(lname)) {
			fullName.append(" ").append(lname);
		}
		return fullName.toString();
	}

	public static String formMigrationKey(String emailId) {
		return RandomStringUtils.randomAlphanumeric(5) + "_" + emailId;
	}

	public static boolean isValidPhoneNumber(String phoneNumber) {
		if (StringUtils.isBlank(phoneNumber)) {
			return false;
		}
		String ePattern = "\\d{10}";
		java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
		java.util.regex.Matcher m = p.matcher(phoneNumber);
		return m.matches();
	}

}
