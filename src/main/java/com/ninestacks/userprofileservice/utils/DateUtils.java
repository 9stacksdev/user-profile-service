package com.ninestacks.userprofileservice.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;


public class DateUtils {

	@SuppressWarnings("unused")
    public static boolean isDateValidByFormat(String dateToValidate, String format) {
		if (dateToValidate == null) {
			return false;
		}

		SimpleDateFormat sdf = new SimpleDateFormat(format);
		sdf.setLenient(false);
		try {
			Date date = sdf.parse(dateToValidate);
		} catch (ParseException e) {
			return false;
		}
		return true;
	}
	
	public static Long getCurrentUnixTimeStamp() {
	    return System.currentTimeMillis() / 1000L;
    }
	
	
    public static Long getUnixTimestampWithZeroTime(String dateString, String format) {
        if(null == dateString || dateString.isEmpty()){
            return null;
        }
        try {
            DateFormat dateFormat = new SimpleDateFormat(format);
            Date date = dateFormat.parse(dateString);
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
            long unixTime = (long)cal.getTimeInMillis()/1000;
            return unixTime;
        } catch (Exception e) {
        }
        return null;
    }
    
    public static Long addDays(long unixTime, int days) {
        try {
            long timestamp = unixTime * 1000;
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(timestamp);
            cal.add(Calendar.DAY_OF_MONTH, days);
            long time = (long)cal.getTimeInMillis()/1000;
            return time;
        } catch (Exception e) {
        }
        return null;
    }

    /**
     * This method is used to convert date in fromDateFormat to date in toDateFormat.
     *
     * @param date the date
     * @param fromDateFormat the from date format
     * @param toDateFormat the to date format
     * @return the formatted date
     */
    public static Long getUnixTimestamp(String dateString, String format) {
        if(null == dateString || dateString.isEmpty()){
            return null;
        }
        try {
            DateFormat dateFormat = new SimpleDateFormat(format);
            Date date = dateFormat.parse(dateString);
            long unixTime = (long)date.getTime()/1000;
            return unixTime;
        } catch (Exception e) {
        }
        return null;
    }
    
    /**
     * This method is used to convert unix timestamp to date in format.
     * @param unixTimestamp
     * @param format
     * @return
     */
    public static String unixToDate(Long unixTimestamp, String format) {  
        try {
            long timestamp = unixTimestamp * 1000;
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            String date = sdf.format(timestamp);
            return date.toString();
        } catch (Exception e) {
        }
        return null;
    }
    
    public static Long addDaysToUnixTimestamp(Long currentUnixTimestamp, long days) {
        return ((currentUnixTimestamp * 1000) + (days*3600*1000))/1000;
    }
    
    /**
     * validate date 
     * @param year
     * @param month
     * @param day
     * @return true/false
     */
    public static boolean isDateValid(int year, int month, int day) {
        boolean dateIsValid = true;
        try {
            LocalDate.of(year, month, day);
        } catch (DateTimeException e) {
            dateIsValid = false;
        }
        return dateIsValid;
    }
    
}
