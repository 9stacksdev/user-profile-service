package com.ninestacks.userprofileservice.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import org.springframework.stereotype.Component;


@Component
public class EncryptionDecryptionUtil {
    
    
    public static String encode(String plainText)
            throws Exception {
        return org.apache.commons.codec.digest.DigestUtils.sha256Hex(plainText);
    }
    
    public String encrypt(String plainText)
            throws Exception {
    	    try{
    		    MessageDigest	md = MessageDigest.getInstance("SHA-256");
    		   byte[] ePass=md.digest(plainText.getBytes());
    		  return  Base64.getEncoder().encodeToString(ePass);
    	    }catch ( NoSuchAlgorithmException nsA){
    	        nsA.printStackTrace();
    	    }
            return null;
    			
    		}

}
