package com.ninestacks.userprofileservice.service.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ninestacks.userprofileservice.data.LoginRequest;
import com.ninestacks.userprofileservice.data.UserProfileRequest;
import com.ninestacks.userprofileservice.entity.AccessToken;
import com.ninestacks.userprofileservice.entity.UserProfile;
import com.ninestacks.userprofileservice.exceptions.EntityAlreadyExistsException;
import com.ninestacks.userprofileservice.exceptions.InvalidArgumentException;
import com.ninestacks.userprofileservice.exceptions.InvalidPasswordException;
import com.ninestacks.userprofileservice.repository.AccessTokenRepository;
import com.ninestacks.userprofileservice.repository.UserProfileRepository;
import com.ninestacks.userprofileservice.service.IUserProfileService;
import com.ninestacks.userprofileservice.transform.UserTransformer;
import com.ninestacks.userprofileservice.utils.AccessTokenUtils;
import com.ninestacks.userprofileservice.utils.EncryptionDecryptionUtil;
import com.ninestacks.userprofileservice.validator.GenericValidator;
import com.ninestacks.userprofileservice.validator.UserProfileRequestValidator;

@Service
public class UserProfileService implements IUserProfileService {

	private Logger logger = LoggerFactory.getLogger(UserProfileService.class);

	@Autowired
	private GenericValidator validatorInvoker;

	@Autowired
	private UserProfileRepository userprofileRepository;

	@Autowired
	private UserTransformer userTransformer;

	@Autowired
	private AccessTokenRepository accessTokenRepository;

	@Override
	public String addUser(UserProfileRequest userProfileRequest) throws Exception {
		if (userProfileRequest == null) {
			return null;
		}

		logger.debug("Entered addUser with" + userProfileRequest.toString());

		// 1. Validate customerDTO request
		validatorInvoker.validateRequest(new UserProfileRequestValidator(), userProfileRequest);

		// 2. Verify if a validated customer already exists with given
		// emailAddress or phoneNumber. If exists then throw exception.
		List<UserProfile> existingUserProfiles = userprofileRepository.findUserProfileByEmailIdOrPhone(
				StringUtils.trim(userProfileRequest.getUserId()), StringUtils.trim(userProfileRequest.getPhoneNumber()),
				StringUtils.trim(userProfileRequest.getEmailAddress()));
		if (existingUserProfiles != null && existingUserProfiles.size() > 0) {
			logger.error("User already exists with provided emailAddress or phoneNumber");
			throw new EntityAlreadyExistsException("User already exists with provided emailAddress or phoneNumber");
		}

		// 3. Convert userprofile request to user profile Object
		UserProfile userProfile = userTransformer.transformUserProfileRequestToUserProfileEntity(userProfileRequest);

		// 6. Save user profile in the database
		if (userProfile != null) {
			userProfile = userprofileRepository.save(userProfile);
			if (userProfile.get_id() == null) {
				logger.error("Error occured while saving user profile into the database");
				throw new Exception("Error occured while adding user profile to the database");
			}
		} else {
			logger.error("Error occured in mapping to profile object");
			throw new Exception("Error occured in mapping to userprofile object");
		}

		return userProfile.get_id();
	}

	@Override
	public String loginUser(LoginRequest loginRequest) throws Exception {
		if (loginRequest == null) {
			return null;
		}
		boolean error = false;

		if (StringUtils.isBlank(loginRequest.getPassword())) {
			error = true;
		}
		if (StringUtils.isBlank(loginRequest.getUserId()) && StringUtils.isBlank(loginRequest.getEmailAddress())
				&& StringUtils.isBlank(loginRequest.getPhoneNumber())) {
			error = true;
		}
		if (error) {
			throw new InvalidArgumentException("Invalid Request");
		}
		logger.debug("Entered loginUSer with" + loginRequest.toString());

		UserProfile userProfile = null;

		if (StringUtils.isNotBlank(loginRequest.getUserId())) {

			userProfile = userprofileRepository.findUserProfileByUserIdAndPassword(loginRequest.getUserId(),
					EncryptionDecryptionUtil.encode(loginRequest.getPassword()));

		} else if (StringUtils.isNotBlank(loginRequest.getPhoneNumber())) {
			userProfile = userprofileRepository.findUserProfileByPhoneNumberAndPassword(loginRequest.getPhoneNumber(),
					EncryptionDecryptionUtil.encode(loginRequest.getPassword()));

		} else if (StringUtils.isNotBlank(loginRequest.getEmailAddress())) {
			userProfile = userprofileRepository.findUserProfileByEmailAddressAndPassword(loginRequest.getEmailAddress(),
					EncryptionDecryptionUtil.encode(loginRequest.getPassword()));

		} else {
			throw new InvalidArgumentException("Invalid Request");
		}

		if (userProfile != null) {
			return getAccessToken(userProfile.get_id(),
					loginRequest.getExpiryInMinutes() == null ? 60L * 24L : loginRequest.getExpiryInMinutes());
		} else {
			throw new InvalidPasswordException("Invalid User or Password ");
		}

	}

	private String getAccessToken(String userId, Long expiryInMinutes) throws Exception {
		if (StringUtils.isBlank(userId) || expiryInMinutes == null) {
			throw new InvalidArgumentException("userId, source and expiryInMinutes cannot be blank");
		}

		String accessTokenId = AccessTokenUtils.createAccessTokenId(userId,"app");
		AccessToken accessTokenDoc = accessTokenRepository.findByAccessTokenId(accessTokenId);
		Long currentUnixTimeStamp = AccessTokenUtils.getCurrentUnixTimeStamp();
		AccessToken newAccessToken = null;
		Long expiresAt = AccessTokenUtils.addSecondsToUnixTimestamp(currentUnixTimeStamp, expiryInMinutes * 60);
		if (accessTokenDoc == null) {
			newAccessToken = new AccessToken(accessTokenId, AccessTokenUtils.createAccessTokenValue(), expiresAt,
					currentUnixTimeStamp);
		} else if (Long.compare(currentUnixTimeStamp, accessTokenDoc.getExpiresAt()) > 0) {
			newAccessToken = accessTokenDoc;
			newAccessToken.setAccessToken(AccessTokenUtils.createAccessTokenValue());
			newAccessToken.setExpiresAt(expiresAt);
			newAccessToken.setLastModifiedAt(currentUnixTimeStamp);
		} else {
			newAccessToken = accessTokenDoc;
			newAccessToken.setExpiresAt(expiresAt);
			newAccessToken.setLastModifiedAt(currentUnixTimeStamp);
		}
		newAccessToken = accessTokenRepository.save(newAccessToken);
		return newAccessToken.getAccessToken();
	}
	
	@Override
    public boolean validateAccessToken(String userId, String source, String accessToken) throws Exception {
        if (StringUtils.isBlank(userId) || StringUtils.isBlank(source) || StringUtils.isBlank(accessToken)) {
            throw new InvalidArgumentException("userId, source and accessToken cannot be blank");
        }
       
        
        String accessTokenId = AccessTokenUtils.createAccessTokenId(userId, source);
        AccessToken accessTokenDoc = accessTokenRepository.findByAccessTokenIdAndAccessToken(accessTokenId, accessToken);
        if (accessTokenDoc == null) {
            return false;
        }
        Long currentTimeStamp = AccessTokenUtils.getCurrentUnixTimeStamp();
        if (Long.compare(currentTimeStamp, accessTokenDoc.getExpiresAt()) > 0) {
            throw new InvalidArgumentException("accessToken expired");
        }
        return true;
    }

	@Override
	public UserProfile getUserById(String userprofileId) {
		if(userprofileId!=null){
		return userprofileRepository.findOne(userprofileId)	;
		}
		return null;
	}

}
