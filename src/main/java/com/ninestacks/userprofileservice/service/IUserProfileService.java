package com.ninestacks.userprofileservice.service;

import com.ninestacks.userprofileservice.data.LoginRequest;
import com.ninestacks.userprofileservice.data.UserProfileRequest;
import com.ninestacks.userprofileservice.entity.UserProfile;
import com.ninestacks.userprofileservice.exceptions.InvalidArgumentException;

public interface IUserProfileService {
	
    

	String addUser(UserProfileRequest userProfileRequest) throws Exception;

	String loginUser(LoginRequest loginRequest) throws InvalidArgumentException, Exception;

	boolean validateAccessToken(String userId, String string, String accessToken)throws InvalidArgumentException, Exception;

	UserProfile getUserById(String userprofileId);
	
	
	


}
