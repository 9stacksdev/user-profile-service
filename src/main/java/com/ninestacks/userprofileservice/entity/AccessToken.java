package com.ninestacks.userprofileservice.entity;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@Document(collection = "access_token")
public class AccessToken implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private String _id;

	private String accessTokenId;

	private String accessToken;

	private Long expiresAt; // unixTimestamp

	private Long lastModifiedAt; // unixTimestamp

	public AccessToken() {
		super();
	}

	public AccessToken(String accessTokenId, String accessToken, Long expiresAt, Long lastModifiedAtFs) {
		super();
		this.accessTokenId = accessTokenId;
		this.accessToken = accessToken;
		this.expiresAt = expiresAt;
		this.lastModifiedAt = lastModifiedAt;
	}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String getAccessTokenId() {
		return accessTokenId;
	}

	public void setAccessTokenId(String accessTokenId) {
		this.accessTokenId = accessTokenId;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public Long getExpiresAt() {
		return expiresAt;
	}

	public void setExpiresAt(Long expiresAt) {
		this.expiresAt = expiresAt;
	}

	public Long getLastModifiedAt() {
		return lastModifiedAt;
	}

	public void setLastModifiedAt(Long lastModifiedAt) {
		this.lastModifiedAt = lastModifiedAt;
	}

}
