package com.ninestacks.userprofileservice.repository.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.ninestacks.userprofileservice.entity.UserProfile;
import com.ninestacks.userprofileservice.repository.UserProfileRepositoryCustom;

public class UserProfileRepositoryImpl implements UserProfileRepositoryCustom {
	@Qualifier("mongoTemplate")
	private MongoOperations mongoOperations;
	public List<UserProfile> findUserProfileByEmailIdOrPhone(String userId,String phoneNumber, String emailAddress) {
		if (StringUtils.isBlank(phoneNumber) && StringUtils.isBlank(emailAddress)) {
			return null;
		}
		final Query query = new Query();
		query.addCriteria(Criteria.where("isValidated").is(Boolean.TRUE));
		if (StringUtils.isNotBlank(phoneNumber) && StringUtils.isNotBlank(emailAddress) && StringUtils.isNotBlank(userId)) {
			query.addCriteria(new Criteria().orOperator(Criteria.where("emailAddress").is(StringUtils.lowerCase(StringUtils.trim(emailAddress))),
					Criteria.where("phoneNumber").is(phoneNumber),Criteria.where("phoneNumber").is(userId.trim().toLowerCase())));
		} else{
			return new ArrayList<UserProfile>();
		}
		return (List<UserProfile>) mongoOperations.find(query, UserProfile.class);
	}

	public MongoOperations getMongoOperations() {
		return mongoOperations;
	}

	@Autowired
	@Qualifier("mongoTemplate")
	public void setMongoOperations(MongoOperations mongoOperations) {
		this.mongoOperations = mongoOperations;
	}

}
