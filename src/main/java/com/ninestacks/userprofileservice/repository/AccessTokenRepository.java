package com.ninestacks.userprofileservice.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.ninestacks.userprofileservice.entity.AccessToken;

public interface AccessTokenRepository extends PagingAndSortingRepository<AccessToken, String> {
    
    public AccessToken findByAccessTokenIdAndAccessToken(String accessTokenId, String accessToken);
    
    public AccessToken findByAccessTokenId(String accessTokenId);
    
    public AccessToken findByAccessToken(String accessToken);

}
