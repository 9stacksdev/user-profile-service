package com.ninestacks.userprofileservice.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.ninestacks.userprofileservice.entity.Ping;
public interface PingRepository extends PagingAndSortingRepository<Ping, String> {
    
}
