package com.ninestacks.userprofileservice.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.ninestacks.userprofileservice.entity.UserProfile;


public interface UserProfileRepository extends PagingAndSortingRepository<UserProfile, String>,UserProfileRepositoryCustom {
	
	public UserProfile findUserProfileByUserIdAndPassword(String userId,String password);
	public UserProfile findUserProfileByPhoneNumberAndPassword(String phoneNumber,String password);
	public UserProfile findUserProfileByEmailAddressAndPassword(String emailAddress,String password);


	
	
	

}
