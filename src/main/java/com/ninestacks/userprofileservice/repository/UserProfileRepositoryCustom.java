package com.ninestacks.userprofileservice.repository;

import java.util.List;

import com.ninestacks.userprofileservice.entity.UserProfile;


public interface UserProfileRepositoryCustom {
	
	
	public List<UserProfile> findUserProfileByEmailIdOrPhone(String userId,String phoneNumber, String emailAddress);

	


	
}
